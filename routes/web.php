<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
	if(!$id = Auth::id()) {
    	return view('welcome');
    }
    return view('user/home');
});

Route::get('/login', function() {
	return '<h1>Please click on the login button on the home page</h1>';
	//irect('/home');
});


Route::get('/register', function () {
    return view('user/register');
});


/*Terms*/
Route::get('/terms-and-conditions', function() {
  return view('termsandconditions');
});

/* privacy policy*/
Route::get('/privacy', function() {
 return view('privacypolicy');
});

