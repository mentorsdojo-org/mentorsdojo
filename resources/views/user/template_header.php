<!doctype html>
<head>
<title> 
<?php
if(!isset($webpage_title)) :
 echo 'Welcome to Mentors Dojo!';
else:
 echo $webpage_title . "| Mentors Dojo";
endif;
?></title>
 <meta name="description" content="Connect mentor and mentee startups" />
<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>stylesheet.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>css/style.css" media="screen" rel="stylesheet" type="text/css" />
  <script src="<?php echo base_url(); ?>js/jquery.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>  
<script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script> <script type="text/javascript">
//<![CDATA[
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas({buttonList : ['bold','italic','ol','ul','link','unlink','xhtml']}) });
  //]]>
</script>

<script src="<?php echo base_url() . 'js/signin.js'; ?>" type="text/javascript"></script>
<script type="text/javascript"> 
	var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-30449315-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=1395136260746513";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>