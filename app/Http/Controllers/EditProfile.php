<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EditProfile extends Controller
{
        public function __invoke($id)
        {
                      return view('profile.edit', ['user' => User::findOrFail($id)]);
        }
}
