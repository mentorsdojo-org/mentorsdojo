<fieldset id="signin_menu">
    <form method="post" id="signin" action="<?php echo site_url('securelogin'); ?>">
      <label for="username">Email</label>
      <input id="username" name="username" value="" title="username" tabindex="1" type="text">
      </p>
      <p>
        <label for="password">Password</label>
        <input id="password" name="password" value="" title="password" tabindex="2" type="password">
      </p>
      <p class="remember">
        <input id="signin_submit" value="Sign in" tabindex="3" type="submit">
        <input id="remember" name="remember_me" value="1" tabindex="4" type="checkbox">
        <label for="remember">Remember me</label>
      </p>
      <p class="forgot"> <a href="<?php echo site_url('passwd'); ?>" id="resend_password_link">Forgot your password?</a> </p>
    </form>
  </fieldset>
