<?php
  include 'templates/header.inc';
?>
<script type="text/javascript"> 
	var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-30449315-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=1395136260746513";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<style>.left-col { padding:15px 15px 15px 15px; } .right-col { padding: 10px 10px 10px 10px;} </style>
<div class="col-md-8 left-col">
<h1 style="color:#1ae;border-bottom:2px #103257 solid">Welcome to Mentors Dojo!</h1>
<h3>What we do</h3>
  <p>We connect with people who need mentoring for their IT career and help mix and match  them with mentors. Mentorsdojo also helps connect people who want to have training in IT and training providers. 
</p>
</div>
<div class="col-md-4 right-col">

<div class="fb-like" style="padding-top:15px;padding-left:15px;" data-href="https://facebook.com/MentorsDojo" data-width="500" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>


<h3>Seminars and Meetups</h3>
<ul class="seminars">
    <li>Visit techmeetupsph.com</li>
</ul>


</div>


</div>
</div>
<?php 
  include 'templates/footer.inc.php';
?>
